'use_strict'
// The Form Data
let formData = [
  // For demonstration purposes,
  // the first form element has been added to the HTML file as a comment
  // compare the input in the HTML file with the contents of this first object
  {
    "type": "text",
    "label": "First Name",
    "id": "user-first-name",
    "icon": "fa-user",
    "options": []
  },
  {
    "type": "text",
    "label": "Last Name",
    "id": "user-last-name",
    "icon": "fa-user",
    "options": []
  },
  {
    "type": "email",
    "label": "Email Address",
    "id": "user-email",
    "icon": "fa-envelope",
    "options": []
  },
  {
    "type": "text",
    "label": "Current Website URL",
    "id": "user-website",
    "icon": "fa-globe",
    "options": []
  },
  {
    "type": "select",
    "label": "Select Language",
    "id": "user-language",
    "icon": "",
    "options": [
      {
        "label": "English",
        "value": "EN"
      },
      {
        "label": "French",
        "value": "FR"
      },
      {
        "label": "Spanish",
        "value": "SP"
      },
      {
        "label": "Chinese",
        "value": "CH"
      },
      {
        "label": "Japanese",
        "value": "JP"
      }
    ]
  },
  {
    "type": "textarea",
    "label": "Your Comment",
    "id": "user-comment",
    "icon": "fa-comments",
    "options": []
  },
  {
    "type": "tel",
    "label": "Mobile Number",
    "id": "user-mobile",
    "icon": "fa-mobile-phone",
    "options": []
  },
  {
    "type": "tel",
    "label": "Home Number",
    "id": "user-phone",
    "icon": "fa-phone",
    "options": []
  }
];

( function() {
  let fields = document.getElementById("fields");
  function createField(type, id, placeholder) {
    if (type === "textarea") {
      return `<textarea type="${type}" id="${id}">${placeholder}</textarea>`
    } else {
      return `<input type="${type}" id="${id}" placeholder="${placeholder}">`
    }
  }
  function createOption(option) {return `<option value="${option.value}">${option.label}</option>`;}
  function createSelector(type, label, id, options) {
    text = `<select id="${id}">`;
    for (let i = 0; i < options.length; i++) {text+= createOption(options[i])}
    return text + "</select>"
  }
  innerHTML = ""
  for (let i = 0; i < formData.length; i++) {
    let element = formData[i];
    if (element.options.length === 0) {
      innerHTML += createField(element.type, element.id, element.label)
    } else {
      innerHTML += createSelector(element.type, element.label, element.id, element.options)
    }
  }
  fields.innerHTML = innerHTML
})();